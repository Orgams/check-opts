#!/bin/bash

#vérification de la version
if [ -z $version_maj ]; then
  version_maj=0
fi

if [ -z $version_min ]; then
  version_min=0
fi

if [ $version_maj -lt 1 ]; then
  echo "Incompatibility of check_opts : diff major version used ($version_maj < 1)"
  exit 3
fi

if [ $version_min -lt 0 ]; then
  echo "Warn check_opts : diff minor version used"
fi

error(){
    echo "ERREUR : parametres invalides !" >&2
    echo "utilisez l'option -H pour en savoir plus" >&2
    exit 1
}

substract(){
  GREY="\\033[38;5;246m"
  BLUE_D="\\033[38;5;20m"
  BLUE_L="\\033[38;5;45m"
  RESET="$(tput sgr0)"

  echo -e "$BLUE_L${0##*/} $GREY> $usage_descr $BLUE_D(/${0#/})$RESET"
}
# Afficher l'aide
usage(){
  # Commencer la ligne indiquant la commande
  usage_script="Usage: ./${0##*/}"

  # Preparer toutes les lignes d'options
  for opt_long in ${options_list[@]}; do

    # Récuperer l'option courte
    opt_short=${opt_long::1}

    # Construire la ligne d'option courante
    usage_opt_tmp="\t--$opt_long ou -$opt_short"

    # Préparer le texte option obligatoire au cas ou l'option est obligatoire
    usage_script_opt_obl="-$opt_short"

    # Ajouter le tag <...> si l'option a une valeur à la ligne d'option courante
    if [[ "$opt_long" =~ .*"$options_avec_valeur".* ]]; then
      usage_opt_tmp+=' <valeur>'
      usage_script_opt_obl+=' <val>'
    fi

    # Ajouter la description de l'option s'il y en a une à la ligne d'option courante
    if [[ ${options_mess["$opt_long"]} ]]; then
      usage_opt_tmp+=" : ${options_mess["$opt_long"]}"
    fi

    # Ajouter les valeurs possible s'il y en a à la ligne d'option courante
    # Construir le nom de la liste
    tab_name="${opt_long}_values"
    # Ajouter les valeurs si la liste existe
    if [[ ${!tab_name} ]]; then
      # Récuperer les valeurs de la liste
      values=$(eval echo "\${$tab_name[@]}")
      # Inserer des virgules entre chaque valeur
      values=${values// /, }
      # Ajouter les valeurs possible à la ligne d'option courante
      usage_opt_tmp+=" (valeurs : $values)"
    fi

    # Ranger la ligne d'option courante dans le bon tableau (obligatoire ou pas)
    if [[ "$opt_long" =~ .*"$options_obligatoires".* ]] ; then
      usage_opt_oblig+="$usage_opt_tmp\n"

      # Ajouter l'option obligatoire à la ligne indiquant la commande
      usage_script+=" $usage_script_opt_obl"
    else
      usage_opt_facul+="$usage_opt_tmp\n"
    fi
  done

  # Finir la ligne indiquant la commande
  usage_script+=" [options]"

  # Afficher l'usage
  echo -e $usage_descr
  echo -e $usage_script
  echo
  echo -e "Options obligatoires : "
  echo -e $usage_opt_oblig
  echo -e "Options facultative : "
  echo -e $usage_opt_facul
}

# Déclarer les tableaux
declare -A options_short_long
declare -A options_mess
declare -A options_used

# Initialisation des messages d'option
options_mess+=(
  ['Help']="pour afficher l'aide"
  ['Substract']="pour afficher la fonction du script"
)

# Ajouter l'option toujours disponible 'help'
options_list+=( Substract Help )
options_avec_valeur+=( ['Substract']=0 ['Help']=0 )

# Commencer de construire la boucle while pour la vérification des options
mkdir /tmp/check_opts/ 2> /dev/null
echo 'while true; do
  case "$1" in' > /tmp/check_opts/while.sh

# Initialiser toues les valeurs pour toutes les options
for opt_long in ${options_list[@]}; do

  # Initialiser les options courtes et longues
  opt_short=${opt_long::1}

  # Associer les options courtes et longues
  # Creer la parti match pour le cas courant
  if [[ ${options_short_long[${opt_short}]} ]]; then
    cas="    --$opt_long) "
    echo "Warn : pas d'option courte pour '--$opt_long' car '-$opt_short' est utilisé pour '--${options_short_long[${opt_short}]}'"
  else
    options_short_long+=( [${opt_short}]=$opt_long )
    cas="    -$opt_short|--$opt_long) "
  fi

  # Ajouter l'option comme utiliser pour le cas courant
  cas+="options_used+=( [$opt_long]=1 ); "

  # Ajouter la création de la variable de l'option pour le cas courant
  if [ ${options_avec_valeur["$opt_long"]} -eq 1 ]; then
    # Ajouter une variable ayant le nom de l'option et la valeur assicier à l'option
    cas+="declare ${opt_long}=\$2; shift 2;;" >> /tmp/check_opts/while.sh

    # Ajouter ':' si l'option a une valeur associé au flitres pour getopt
    opt_long=$opt_long:
    opt_short=$opt_short:
  else

    # Ajouter le cas de l'option au while
    cas+="declare "${opt_long}"=1; shift;;" >> /tmp/check_opts/while.sh
  fi

  # Ajouter la cas courant au ficher pour la future boucle while
  echo $cas >> /tmp/check_opts/while.sh

  # Ajouter l'options aux chaines pour getopt
  opt_spec_long=$opt_spec_long$opt_long,
  opt_spec_short=$opt_spec_short$opt_short
done

# Enlever la ',' de fin de chaine
opt_spec_long=${opt_spec_long::-1}

# Finir de construire la boucle while
echo '    --) shift; break;;
    *) error; shift;;
  esac
done' >> /tmp/check_opts/while.sh

# Recuperer les options correctement trier
# -o : options courtes
# -l : options longues
options=$(getopt -u -o $opt_spec_short -l $opt_spec_long -- "$@")

# Eclatement de $options en $1, $2...
set -- $options

# Utilisation de la boucle while construite
source /tmp/check_opts/while.sh

# Afficher l'aide si demander
if [[ $Help ]]; then
  usage
  exit 0
fi
if [[ $Substract ]]; then
  substract
  exit 0
fi

# Vérifier que toutes les options obligatoires ont été fournies
for opt_long in ${options_obligatoires[@]}; do
  if [[ ${options_used["$opt_long"]} -ne 1 ]]; then
    # Construire le nom de la liste
    tab_name="${opt_long}_values"
    if [[ ${!tab_name} ]]; then
      values=$(eval echo "\${$tab_name[@]}")
      # Inserer des virgules entre chaque valeur
      values=${values// /, }
      values=" (valeurs : $values)"
    fi
    echo -e "${0##*/} : Il manque l'option --$opt_long ${options_mess["$opt_long"]}$values" >&2;
    error_option=1
  fi
done

# Vérifier que les options utilisé qui doivent avoir une valeur et qui ont des valeurs défini n'en on pas d'autre
for opt_long in ${options_avec_valeur[@]}; do
  if [[ ${options_used["$opt_long"]} -eq 1 ]] ; then
    # Construire le nom de la liste
    tab_name="${opt_long}_values"
    # Vérifier que la valeur est autorisé si une contrainte est défini
    if [[ ${!tab_name} ]]; then
      values=$(eval echo "\${$tab_name[@]}")
      # Afficher un message d'erreur si la valeur n'est pas valide
      if ! [[ " $values " = *" ${!opt_long} "* ]]; then
        echo -e "${0##*/} : L'option --$opt_long doit avoir une valeur dans : ${values// /, }";
        error_option=1
      fi
    fi
  fi
done

if [[ $error_option ]]; then
  exit 2
fi
