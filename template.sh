#!/bin/bash

. ~/.bash_env

# =======================================================================
# TRAITEMENT DES PARAMETRE
# =======================================================================

# Initialisation des options possibles
options_list=( vars var2 )

# Initialisation des options en cle et leurs type en valeur, 1 pour obligatoire et 0 sinon
options_obligatoires=( vars )

# Initialisation des options en cle et leurs type en valeur, 1 avec valeur associé et 0 sinon
options_avec_valeur=( vars )

# Initialisation du tableau indiquant les options qui ont des valeurs

usage_descr="Permet d'avoir un model pour créer d'autre script"

declare -A options_mess=(
  ['vars']="définir vars"
  ['var2']="deuxième var"
)

vars_values=("val1" "val2")

# Appeler check_opts pour analyser les params en présisan la version du script
version_maj=1
version_min=0
. $sh_script/lib/check_opts.sh

# =======================================================================
# INITIALISATION
# =======================================================================

# =======================================================================
# MAIN
# =======================================================================

if [[ $vars ]] ; then
	echo "vars ok"
else
	echo "vars ko"
fi

if [[ $vars == "val1" ]] ; then
	echo "valeur 1"
fi

if [[ $vars == "val2" ]] ; then
	echo "valeur 2"
fi
