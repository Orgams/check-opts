# Check opts
Utilitaire permetant de déclarer les arguments possibles d'un script bash en utilisant des tableaux avec des clef-valeur.

## Principe
Les options possible sont définie par un tableau, les options courtes sont généreres
Avec les informations données dans des tableaux des vérifications vont être faites et l'aide va être généré (-h)
Après le passage du script une variable va être initalisié pour chaque option passer en CLI contenant soit la valeur associé ou si c'est un flag la valeur '1'

## Tableaux à initialiser
| Nom du tableau | Description |
| ------ | ------ |
| options_list | indique les options utiliser par le script |
| options_obligatoires | indique les options obligatoires (erreur si non présent) |
| options_avec_valeur | indique les options avec valeur (erreur si pas de valeur) |
| options_mess | indique le message asocier à l'option (permet de générer le help) |

## Variables à initialiser
|Nom de la variable | Description|
|------|------|
|usage_descr| description du script (permet de générer le help)|
|{var}_values| valeurs que peut prendre l'option {var}|

## Tableau des options
Dans ce tableau nous devons indiquer toutes les options utiliser dans le script.
Cela permet de générer l'aide (voire également)
```bash
# Initialisation des options possibles
options_list=( vars var2 )
```

## Prise en compte de l'option courte

les options courtes sont généreres avec la premiere lettre de l'option longue (trouver dans ) si elle n'est pas encore prise.
Si une lettre est déjà prise warning affichier
```
Warn : pas d'option courte pour '--var2' car '-v' est utilisé pour '--vars'
```

## Tableau options obligatoires
Dans ce tableau nous devons indiquer quels options sont obligatoires.
Si le script est lancé sans mettre une option obligatoire un message d'erreur est affiché.
Exemple d'initialisation :
```bash
# Initialisation des options en cle et leurs type en valeur, 1 pour obligatoire et 0 sinon
options_obligatoires=( vars )
```

## Tableau options avec valeur
Dans ce tableau nous devons indiquer quels option ont une valeur associé.
Si le script est lancé avec une option devent avoir un valeur sans valeur un message d'erreur est affiché.
Exemple d'initialisation :
```bash
# Initialisation du tableau indiquant les options qui ont des valeurs
declare -A options_avec_valeur=( ['vars']=1, ['var2']=0 )
```

## Tableau options message
Dans ce tableau nous devons décrire le but de l'option.
Cela sera utilié dans l'aide
Exemple d'initialisation :
```bash
declare -A options_mess=(
  ['vars']="définir vars"
  ['var2']="deuxième var"
)
```

## Variable de description du script
Dans cette variable nous devons décrire le but du script.
Cela sera utilié dans l'aide
Exemple d'initialisation :
```bash
usage_descr="Permet d'avoir un model pour créer d'autre script"
```
